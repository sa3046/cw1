CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic
.PHONY : all
all : program
program: main.cpp event.o booking.o livemusic.o save_load.o film.o standupcomedy.o
	$(CXX) $(CXXFLAGS) -o $@ $^
event.o: event.cpp event.h
	$(CXX) $(CXXFLAGS) -c $<
booking.o: booking.cpp booking.h
	$(CXX) $(CXXFLAGS) -c $<
livemusic.o: livemusic.cpp livemusic.h
	$(CXX) $(CXXFLAGS) -c $<
save_load.o: save_load.cpp save_load.h
	$(CXX) $(CXXFLAGS) -c $<
film.o: film.cpp film.h
	$(CXX) $(CXXFLAGS) -c $<
standupcomedy.o: standupcomedy.cpp standupcomedy.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean :
	$(RM) *.o
	$(RM) program
