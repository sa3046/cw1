#include <string>
#include <vector>
#include "booking.h"

#ifndef _EVENT_H_
#define _EVENT_H_
class Event {
    public:
        Event();
        Event(std::string name);
        std::string getName();
        int getmaxseats();
        bool getseated();
        std::vector<Booking> getbookings();
        void bookevent(Booking booking);
        void cancelbooking(std::string booking);


    protected:
        bool seated;
        int maxseats;

    private:
        std::string name;
        std::vector<Booking> bookings;
};
#endif
