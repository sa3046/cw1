#include "event.h"
#ifndef _FILM_H_
#define _FILM_H_
class Film: public Event{
  public:
    Film(std::string name);
    int getfilmtype();
  private:
    int filmtype;
    int seatavailable;
};
#endif
