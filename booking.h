#include <string>
#include <vector>
#ifndef _BOOKING_H_
#define _BOOKING_H_
class Booking {
    public:
        Booking(std::string customer, std::string event);
        std::string getcustomer();
    private:
        std::string customer;
        std::string event;
};
#endif
