#include "standupcomedy.h"
Standupcomedy::Standupcomedy(std::string name) : Event(name) {
    // Setting standup comedy event defaults.
    this->freeseats = 200;
    this->maxseats = 200;
    this->seated = true;
}