#include "event.h"
#ifndef _SAVE_LOAD_H_
#define _SAVE_LOAD_H_
class SaveLoad {
    public:
        SaveLoad();
        void save(std::vector<Event> events);
        std::vector<Event> load();
};
#endif
