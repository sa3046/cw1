#include <iostream>
#include "event.h"

Event::Event(std::string name) {
    this->name = name;
}

std::string Event::getName() {
    return this->name;
}

int Event::getmaxseats() {
    return this->maxseats;
}

bool Event::getseated() {
    return this->seated;
}

void Event::bookevent(Booking booking) {
    this->bookings.push_back(booking);
}

std::vector<Booking> Event::getbookings() {
    return this->bookings;
}